"use strict";

const request = require('request');
const parseString = require('xml2js').parseString;
const path = require("path");
const express = require("express");
const config = require("./config");
const bcrypt = require('bcryptjs');

const Sequelize = require('sequelize');
const database = require('./database');
const mailgun = require('mailgun-js')({apiKey: config.mailgun_key, domain: config.mailgun_domain});

//const UsersController = require("./api/users/users.controller");
//const AddBookController = require("./api/addbook/addbook.controller");

const API_USERS_URI = "/api/users";
const API_ADDBOOK_URI = "/api";

module.exports = function(app, database, passport){
    /* declare const */
    //GOOREADS API
    const GOODREADS_SEARCH_URL = "https://www.goodreads.com/search";
    const GOODREADS_BOOKS_URL = "https://www.goodreads.com/book/show.xml";
    const GOODREADS_KEY = "vqR2AIjf0ZTjzE8rq9rEA";

    const HOME_PAGE = "/index.html#!/home";
    const SIGNIN_PAGE = "/index.html#!/form";
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const LIBS_FOLDER = path.join(CLIENT_FOLDER + "/bower_components");

    app.use("/libs", express.static(LIBS_FOLDER));
    app.use(express.static(CLIENT_FOLDER));
    
    app.get(API_ADDBOOK_URI + '/searchbook/:searchStr', function(req,res){
        var goodReadsRequest = GOODREADS_SEARCH_URL + 
            "?key=" + GOODREADS_KEY + 
            "&q=" + req.params.searchStr;
        request(goodReadsRequest, 
        function (error, response, body) {
            //console.log('body:', body); 
            parseString(body, function (err, result) {
                //console.log(JSON.stringify(result.GoodreadsResponse.search));
                res.json(result.GoodreadsResponse.search);
            });
        });
    });
    //app.get(API_ADDBOOK_URI + '/getbookinfo/:id', API_ADDBOOK_URI.getbookinfo);
    app.get(API_ADDBOOK_URI + "/getbookinfo/:id", function(req, res) {
        var goodReadsRequest = GOODREADS_BOOKS_URL + 
            "?key=" + GOODREADS_KEY + 
            "&id=" + req.params.id;

        request(goodReadsRequest, 
                function (error, response, body) {
                    //console.log('body:', body); 
                    parseString(body, function (err, result) {
                        //console.log(JSON.stringify(result.GoodreadsResponse.search));
                        res.json(result.GoodreadsResponse);
                    });
                });
    });

    app.get("/api/users/books/:userid", function(req,res){
        // var books = database.Books;
        var Users = database.Users;
        var Book_Users = database.Book_Users;
        var Books = database.Books;
        // var userid = req.session.userProfile.userid;        
        var userid = 2;                

        console.log("getting books from userid," + userid);

        Users
            .findAll({
                where: {userid: userid}
                ,limit: 1
                ,include: [{
                    model: Book_Users
                    // ,limit: 20
                    ,include: [Books]
                }]
            })
            .then(function(result){
                res
                    .status(200)
                    .json(result);
                    console.log(result);
            })
            .catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    });

    /*  /api/users/register --> Register User  */
    app.post("/api/users/register", function(req, res) {
            var password = req.body.user.password;
            var hashpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            // var email_count = 0;
            // var validation_passflag = false;
            database.Users
                //server validation
                
                //end of server validation
                .create({
                    email: req.body.user.email,
                    //password: req.body.user.password,
                    password: hashpassword,
                    firstname: req.body.user.firstname,
                    lastname: req.body.user.lastname,
                    gender: req.body.user.gender,
                    dob: req.body.user.dob,
                    country: req.body.user.country
                    //Roleid: 2
                })
                .then(function(results) {
                    //email to user after add user into Table: Users
                    // console.log(">>>>config.register_email>>>>>");
                    // console.log(config.register_email);
                    var data = {
                        from: config.register_email.from,
                        to: req.body.user.email,
                        subject: config.register_email.subject,
                        text: config.register_email.email_text
                    };
                    mailgun.messages().send(data, function (error, body){
                        console.log(body);
                    })
                    //
                    console.log(">>>>>/api/users/register >>>>");
                    console.log(">>>>>>>>>>>results>>>>>>>>>>>>>");
                    console.log(results);
                    console.log(">>>>>>>End of results>>>>>>>>>>>>");
                    res
                        .status(200)
                        .json(results);
                })
                .catch(function(err) {
                    res
                        .status(501)
                        .json(err) 
                });
            
    }); //Register User

    /*  /api/users/updateprofile --> Update Profile  */
    app.post("/api/users/updateprofile", function(req, res) {
        var user = database.Users;

        var userid = req.body.user.userid;
        
        var password = req.body.user.password;
        var hashpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        
        var email = req.body.user.email;
        var firstname = req.body.user.firstname;
        var lastname = req.body.user.lastname;
        
        var gender = req.body.user.gender;
        var dob = req.body.user.dob;
        var country = req.body.user.country;
        var profilephoto = req.body.user.profilephoto;
        // console.log("req.body.user>>>>>>");
        // console.log(req.body.user);
        // database.Users
        user
            .find({where: { userid: userid}})
            .then(function(rUser){
                console.log(">>>>>rUser >>>>");
                console.log(rUser);
                rUser.updateAttributes({
                    email: email,
                    firstname: firstname,
                    lastname: lastname,
                    password: hashpassword,
                    gender: gender,
                    dob: dob,
                    country: country,
                    profilephoto: profilephoto
                })
                .then(function(results) {
                    //console.log(">>>>/api/users/updateprofile >>>>>");
                    //console.log(results);
                    res
                        .status(200)
                        .json(rUser);
                })
                .catch(function(err) {
                    res
                        .status(501)
                        .json(err) 
                });
            })
            .catch(function(err){
                res
                .status(501)
                .json(err) 
            })
            
    }); //Update Profile

    /*  /api/users/getuserinfo --> Retrieve User Profile  */
    app.get("/api/users/getuserinfo/:email", function(req, res) {
        var email = req.params.email;
        console.log("email >>> %s", email);
        database.Users
            .findAll({
                where: {
                    email: email
                }
            })
            .then(function(results){
                //console.log(">>>>results>>>>");
                //console.log(results[0].dataValues);
                res
                .status(200)
                .json(results[0].dataValues)
            })
            .catch(function(err){
                res
                .status(501)
                .json(err) 
            })
    }); //Retrieve User Profile

    /*  /api/users/checkemail --> Check Email Exist  */
    app.get("/api/users/checkemail/:email", function(req, res) {
        var email = req.params.email;
        var email_count = 0;
        
         database.Users
            .findAll({
                attributes: [[Sequelize.fn('COUNT', Sequelize.col('email')), 'email_count']],
                where: {
                    email: email
                }
            })
            .then(function(results) {
                console.log(">>>>/api/users/checkemail/>>>>>");
                
                email_count = results[0].dataValues.email_count;
                console.log("email_count for %s >>> " , email , " >> " ,email_count);
                res
                    .status(200)
                    .json(email_count);
            })
            .catch(function(err) {
                res
                    .status(501)
                    .json(err) 
            });
        
    }); //Check Email Exist

    // app.post("/security/login", passport.authenticate("local", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE
    // }));

    app.post("/security/login",
        function(req, res, next) {
            passport.authenticate('local',
                function(err, user, info) {
                    if (err) {
                        return next(err);
                    };
                    if (!user) {
                        return res.status(401).json({err: info});
                    };
                    req.login(user, function(err) {
                        if (err) {
                            return res.status(500).json({err: 'Could not log in user'});
                        } else {
                            // req.session.auth = true;
                            // values of 'user' will be assigned to req.user
                            res.status(200).json({status: 'Login successful!'});
                        };
                    });
                })(req, res, next);
            });


    app.get("/status/user", function(req, res){
        var status = null;
        if(req.user){
            status = req.user;
            console.log(">>>get Status>>>>, status>>>>>");
            console.log(status);
        }

        res.status(200).json(status);
    });

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + HOME_PAGE);
    });

    // Facebook Authentication
    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }

    app.use(function(req, res, next) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });
    

};