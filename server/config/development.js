"use strict";

const domain_name = "http://localhost:3000";

module.exports = {
    mysql: process.env.MYSQL_DATABASE_URL || "mysql://bookcat:meow@localhost/bookcat?reconnect=true",
    domain_name: domain_name,
    aws: {
        id: "AKIAIFURL2BZGGDQKWHQ",
        key: "GlXBE0BLmbGvLECfzqPEW6VQadioaoxy49Wpw1CG",
        url: "https://assessment3-fileupload.s3.amazonaws.com",
        bucket: "assessment3-fileupload",
        region: "ap-southeast-1"
    },
    mailgun_key: "key-fe808dda2bc1f5d1bab7ec852aa56669",
    mailgun_domain: "sandbox7cabdb58c15943d0b534c80dee2632f5.mailgun.org",
    register_email: {
        from: "Book Kitty <noreply@bookcat.io>",
        subject: "Welcome to BookCat!",
        email_text: "Hello! Thank you for registering!"
    },
    port: process.env.PORT || 3000,
    seed: false,
    Facebook_key: process.env.FACEBOOK_KEY || "118466742176058",
    Facebook_secret: process.env.FACEBOOK_SECRET_KEY || "48fddfce51e41fb7e1d30e54015d066a",
    Facebook_callback_url: process.env.FACEBOOK_CALLBACK_URL || domain_name + "/oauth/facebook/callback"
};