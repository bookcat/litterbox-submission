var bcrypt = require('bcryptjs');
var config = require("./config");
var database = require("./database");

var Users = database.Users;
var Roles = database.Roles;
var Books = database.Books;
var Publishers = database.Publishers;
var Book_Users = database.Book_Users;

module.exports = function() {
    if (config.seed) {

        var hashpassword = bcrypt.hashSync("Abc1@345", bcrypt.genSaltSync(8), null);
        
        Roles
        .create({
            roleid: 1,
            rolename: "admin",
            desc: "admin"
        })
        .then(function(user) {
            //console.log(user);
        }).catch(function() {
            console.log("Error", arguments)
        })
        Roles
            .create({
                roleid: 2,
                rolename: "user",
                desc: "user"
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        Roles
            .create({
                roleid: 3,
                rolename: "disabled",
                desc: "assigned to deleted user"
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        
        Users
            .create({
                email: "charmainetan@gmail.com",
                firstname: "Charmaine",
                lastname: "Tan",
                password: hashpassword,
                gender: "f",
                dob: "1987-02-05",
                country: "Singapore",
                profilephoto: null,
                Roleid: 1,
                // createdat: new Date(),
                // updatedat: null,
                // deletedat: null
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })

        Users
            .create({
                email: "hosc11@gmail.com",
                firstname: "Siaw Chin",
                lastname: "Ho",
                password: hashpassword,
                gender: "f",
                dob: "1978-08-19",
                country: "Singapore",
                profilephoto: null,
                Roleid: 1,
                // createdat: new Date(),
                // updatedat: null,
                // deletedat: null
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        
        Users
            .create({
                email: "keira@gmail.com",
                firstname: "Keira Christina",
                lastname: "Knightley",
                password: hashpassword,
                gender: "f",
                dob: "1985-03-26",
                country: "England",
                profilephoto: null,
                Roleid: 2,
                // createdat: new Date(),
                // updatedat: null,
                // deletedat: null
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        
        Users
            .create({
                email: "thomas@gmail.com",
                firstname: "Thomas Jeffrey",
                lastname: "Hanks",
                password: hashpassword,
                gender: "m",
                dob: "1956-07-09",
                country: "United States",
                profilephoto: null,
                Roleid: 2,
                // createdat: new Date(),
                // updatedat: null,
                // deletedat: null
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        Users
            .create({
                email: "audrey@gmail.com",
                firstname: "Audrey",
                lastname: "Hepburn",
                password: hashpassword,
                gender: "f",
                dob: "1929-05-04",
                country: "Switzerland",
                profilephoto: null,
                Roleid: 3,
                // createdat: '1950-01-01',
                // updatedat: '1962-01-01',
                // deletedat: '1993-01-01'
            })
            .then(function(user) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
       
        Publishers
            .create({
                publishername: "Harcourt, Inc."
            })
            .then(function(publishers) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })

        Books
            .create({
                goodreadsid:157993,
                isbn10: "0156012197",
                isbn13: "9781930900240",
                title: "The Little Prince",
                author: "Antoine de Saint-Exupéry",
                format: "Paperback",
                imgurl: "https://images.gr-assets.com/authors/1330853515p5/1020792.jpg",
                synopsis: "Moral allegory and spiritual autobiography, The Little Prince is the most translated book in the French language. With a timeless charm it tells the story of a little boy who leaves the safety of his own tiny planet to travel the universe, learning the vagaries of adult behaviour through a series of extraordinary encounters. His personal odyssey culminates in a voyage to Earth and further adventures.",
                Pubid: "1"
                
            })
            .then(function(books) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
        
        Book_Users
            .create({
                bookid: 1,
                userid: 2,
                status: 'read'
            })
            .then(function(books) {
                //console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })
    }
};