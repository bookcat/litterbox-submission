//Load libraries
const q = require("q");
const fs = require('fs');
const path = require("path");
const mysql = require("mysql");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);
const express = require("express");

const Sequelize = require("sequelize");
const passport   = require('passport');  //to handle authentication
const passportSession = require("passport-session");

var moment = require("moment");

const NODE_PORT = process.env.NODE_PORT || 3000;

var app = express();

app.use(cookieParser());

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

const credentials = require('./credentials.js');

app.use(session({
        secret: credentials.sessionSecret,
        // resave: false,
        // saveUninitialized: true
    })
);

app.use(passport.initialize());
app.use(passport.session());

var config = require("./config");
var db = require("./database");

require("./auth.js")(app, passport);
require("./routes")(app, db, passport);

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + config.port);
});