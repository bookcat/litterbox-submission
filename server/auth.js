'use strict';

var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require("passport-facebook").Strategy
var bcrypt = require('bcryptjs');

var User = require("./database").Users;
//var AuthProvider = require("./database").AuthProvider;
var config = require("./config");

//Setup local strategy
module.exports = function(app, passport) {

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function(user, done) {
            User.findOne({
                where: {
                    email: user.email
                }
            }).then(function(result) {
                if (result) {
                    done(null, user);
                }
            }).catch(function(err) {
                done(null, false);
            });
    });
    
    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    function authenticate(req, email, password, done) {

        User.findOne({
            where: {
                email: email
            }
        }).then(function(result) {
            if (!result) {
                return done(null, false, { loginMessage: 'Invalid Email' });
            } else {
                if (bcrypt.compareSync(password, result.password)) {

                    return done(null, result);
                } else {
                    //console.log("password incorrect");
                    return done(null, false, { loginMessage: 'Invalid Password' });
                }
            }
        }).catch(function(err) {
            return done(err, false);
        });
    }
    
    passport.use('local-login',new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
        passReqToCallback : true // allows us to pass back the entire request to the callback
    }, authenticate));

    // =========================================================================

    function verifyCallback(accessToken, refreshToken, profile, done) {
        console.log(profile);
        if (profile.provider  === 'facebook') {
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            User.findOrCreate({ where: { email: email }, defaults: { username: email, email: email, password: null, name: displayName } })
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    // AuthProvider.findOrCreate({
                    //         where: { userid: user.id, providerType: provider_type },
                    //         defaults: { providerId: id, userId: user.id, providerType: provider_type, displayName: displayName }
                    //     })
                    //     .spread(function(provider, created) {
                    //         console.log(provider.get({
                    //             plain: true
                    //         }));
                    //         console.log(created);
                    //     });
                    done(null, user);
                });
        } else {
            done(null, false);
        }
    }

    passport.use(new FacebookStrategy({
        clientID: config.Facebook_key,
        clientSecret: config.Facebook_secret,
        callbackURL: config.Facebook_callback_url,
        profileFields: ['id', 'displayName', 'photos', 'email']
    }, verifyCallback))    

};