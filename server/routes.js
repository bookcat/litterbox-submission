"use strict";

const request = require('request');
const parseString = require('xml2js').parseString;
const path = require("path");
const express = require("express");
const config = require("./config");
const bcrypt = require('bcryptjs');

const Sequelize = require('sequelize');

const database = require('./database');
const mailgun = require('mailgun-js')({apiKey: config.mailgun_key, domain: config.mailgun_domain});

const API_USERS_URI = "/api/users";
const API_PRO_URI = "/app/protected";
const API_ADDBOOK_URI = "/api";

module.exports = function(app, database, passport){
    /* declare const */
    //GOOREADS API
    const GOODREADS_SEARCH_URL = "https://www.goodreads.com/search";
    const GOODREADS_BOOKS_URL = "https://www.goodreads.com/book/show.xml";
    const GOODREADS_KEY = "vqR2AIjf0ZTjzE8rq9rEA";

    const HOME_PAGE = "/app/protected/home.html";
    const SIGNIN_PAGE = "/index.html#!/login";
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const LIBS_FOLDER = path.join(CLIENT_FOLDER + "/bower_components");

    app.use("/libs", express.static(LIBS_FOLDER));
    app.use(express.static(CLIENT_FOLDER));

    /*  /api/users/register --> Register User  */
    app.post(API_USERS_URI + "/register", function(req, res) {
            var password = req.body.user.password;
            var hashpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            // var email_count = 0;
            // var validation_passflag = false;
            database.Users
                //server validation
                
                //end of server validation
                .create({
                    email: req.body.user.email,
                    //password: req.body.user.password,
                    password: hashpassword,
                    firstname: req.body.user.firstname,
                    lastname: req.body.user.lastname,
                    gender: req.body.user.gender,
                    dob: req.body.user.dob,
                    country: req.body.user.country
                    //Roleid: 2
                })
                .then(function(results) {
                    //email to user after add user into Table: Users
                    // console.log(">>>>config.register_email>>>>>");
                    // console.log(config.register_email);
                    // var data = {
                    //     from: config.register_email.from,
                    //     to: req.body.user.email,
                    //     subject: config.register_email.subject,
                    //     text: config.register_email.email_text
                    // };
                    // mailgun.messages().send(data, function (error, body){
                    //     console.log(body);
                    // })
                    //
                    console.log(">>>>>/api/users/register >>>>");
                    console.log("After Sign Up >>>>>>>>>>>results>>>>>>>>>>>>>");
                    console.log(results.dataValues);
                    console.log(">>>>>>>End of results>>>>>>>>>>>>");
                    // console.log("After Sign Up, req.body.user >>>");
                    // console.log(req.body.user);
                    req.session.userProfile.user.push(results.dataValues);
                    res
                        .status(200)
                        .json(results);
                })
                .catch(function(err) {
                    
                    res
                        .status(501)
                        .json(err) 
                });
            
    }); //Register User

     /*  /api/users/updateprofile --> Update Profile  */
     app.post(API_USERS_URI + "/updateprofile", function(req, res) {
        var user = database.Users;
        var userProfile = req.body.userProfile;

        var userid = userProfile.userid;
        
        // var password = userProfile.password;
        // var hashpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        
        var email = userProfile.email;
        var firstname = userProfile.firstname;
        var lastname = userProfile.lastname;
        
        var gender = userProfile.gender;
        var dob = userProfile.dob;
        var country = userProfile.country;
        var profilephoto = userProfile.profilephoto;
        // console.log("req.body.user>>>>>>");
        // console.log(req.body.user);
        // database.Users
        user
            .find({where: { userid: userid}})
            .then(function(rUser){
                console.log(">>>>>rUser >>>>");
                console.log(rUser);
                rUser.updateAttributes({
                    email: email,
                    firstname: firstname,
                    lastname: lastname,
                    // password: hashpassword,
                    gender: gender,
                    dob: dob,
                    country: country,
                    profilephoto: profilephoto
                })
                .then(function(results) {
                    //console.log(">>>>/api/users/updateprofile >>>>>");
                    //console.log(results);
                    res
                        .status(200)
                        .json(rUser);
                })
                .catch(function(err) {
                    res
                        .status(501)
                        .json(err) 
                });
            })
            .catch(function(err){
                res
                .status(501)
                .json(err) 
            })
            
    }); //Update Profile


    /*  /api/users/getuserinfo --> Retrieve User Profile  */
    app.get(API_USERS_URI + "/getuserinfo/:email", function(req, res) {
        var email = req.params.email;
        console.log("email >>> %s", email);
        database.Users
            .findAll({
                where: {
                    email: email
                }
            })
            .then(function(results){
                //console.log(">>>>results>>>>");
                //console.log(results[0].dataValues);
                res
                .status(200)
                .json(results[0].dataValues)
            })
            .catch(function(err){
                res
                .status(501)
                .json(err) 
            })
    }); //Retrieve User Profile

    /*  /api/users/getuserprofile/:userid --> Retrieve User Profile with Userid  */
    app.get(API_USERS_URI + "/getuserprofile/:userid", function(req, res) {
        
        var userid = req.session.userProfile.user[0].userid;
        console.log("userid >>> %s", userid);
        database.Users
            .findAll({
                where: {
                    userid: userid
                }
            })
            .then(function(results){
                //console.log(">>>>results>>>>");
                //console.log(results[0].dataValues);
                res
                .status(200)
                .json(results)
            })
            .catch(function(err){
                res
                .status(501)
                .json(err) 
            })
    }); //Retrieve User Profile
    
    /*  /api/users/checkemail --> Check Email Exist  */
    app.get(API_USERS_URI + "/checkemail/:email", function(req, res) {
        var email = req.params.email;
        var email_count = 0;
        
         database.Users
            .findAll({
                attributes: [[Sequelize.fn('COUNT', Sequelize.col('email')), 'email_count']],
                where: {
                    email: email
                }
            })
            .then(function(results) {
                console.log(">>>>/api/users/checkemail/>>>>>");
                
                email_count = results[0].dataValues.email_count;
                console.log("email_count for %s >>> " , email , " >> " ,email_count);
                res
                    .status(200)
                    .json(email_count);
            })
            .catch(function(err) {
                res
                    .status(501)
                    .json(err) 
            });
        
    }); //Check Email Exist

    app.use(function(req, res, next) {
        //new session
        if (!req.session.userProfile) {
            console.log(">>> creating a new session for req.session.userProfile");
            req.session.userProfile = {
                user:[]
            };
        }
        if (!req.session.Error) {
            console.log(">>> creating a new session for req.session.Error");
            req.session.Error = {
                message: ""
            };
        }
        next();
    });  // Create new session for req.session.userProfile & req.session.Error
    
    app.get(API_PRO_URI + '/home',isLoggedIn, function(req, res){
        
        return res.redirect(HOME_PAGE);
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // app.post("/security/login", passport.authenticate("local-login", {   
    //     successRedirect: '/protected/home', // redirect to the /app/protected/home.html
    //     failureRedirect: '/login'  //redirect back to the index page if there is an error
    // })); // Login

    app.post("/security/login", function(req, res, next) {
        passport.authenticate('local-login',
            function(err, user, info) {
                if (err) {
                    return next(err);
                };
                if (!user) {
                    //console.log("info.loginMessage >>>> ", info.loginMessage);
                    req.session.Error.message = info.loginMessage;
                    return res.redirect('/login');
                    //return res.status(401).json({err: info.loginMessage});
                };
                req.login(user, function(err) {
                    if (err) {
                        //console.log("info.loginMessage >>>> ", info.loginMessage);
                        req.session.Error.message = info.loginMessage;
                        return res.redirect('/login');
                        //return res.status(500).json({err: info.loginMessage});
                    } else {
                        req.session.userProfile.user.push(req.user);
                        console.log("After login, req.session.userProfile.user >>> ");
                        console.log(req.session.userProfile.user);
                        return res.redirect('/app/protected/home');
                        //return res.status(200).json({status: 'Login successful!'});
                    };
                });
            })(req, res, next);
    });

    app.get('/login', function(req, res) {
        return res.redirect(303, '/');
    });

    app.get('/getErrorMessage', function(req, res) {
        var errMessage = "";
        //console.log("/getErrorMessage --> req.session.Error.message >>>", req.session.Error.message);
        if (req.session.Error.message != ""){
            errMessage = req.session.Error.message;
            //console.log("/getErrorMessage --> errMessage >>>", errMessage);
            req.session.Error.message="";
        }
        res.type('text/plain');
        return res.send(errMessage);
    })
    // =========================================================================

    // Facebook Authentication
    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        return res.redirect('/');
        //res.send(req.user).end();
    });


    app.get("/getUserLoggedInProfile", isLoggedIn, function(req, res){
        console.log("###### in '/getUserLoggedInProfile' @@@@ req.session.userProfile ######");
        console.log(req.session.userProfile.user);
        res.json(req.session.userProfile.user);
    });

    app.get("/getUserSignUpProfile", function(req, res){
        console.log("###### in '/getUserLoggedInProfile' @@@@ req.session.userProfile ######");
        console.log(req.session.userProfile.user);
        res.json(req.session.userProfile.user);
    });

    app.get("/status/user", function(req, res) {
        var status = "";

        //if (req.user) {
        if (req.session.userProfile.user){
            // console.log("%%%%%###$$$req.session.userProfile>>>>");
            // console.log(req.session.userProfile.user);
            // console.log("++++++++++++++++++++++++++++++++++++++++");
            status = req.session.userProfile.user[0];
        }
        //console.info("@@@@@@@@@@@@@@@@@req.session.userProfile.user --> " + status);
        res.json(status).end();
    });

    app.get(API_ADDBOOK_URI + "/searchbook/:searchStr", function(req,res){
        var goodReadsRequest = GOODREADS_SEARCH_URL + 
            "?key=" + GOODREADS_KEY + 
            "&q=" + req.params.searchStr;
        request(goodReadsRequest, 
        function (error, response, body) {
            //console.log('body:', body); 
            parseString(body, function (err, result) {
                //console.log(JSON.stringify(result.GoodreadsResponse.search));
                res.json(result.GoodreadsResponse.search);
            });
        });
    });

    //app.get(API_ADDBOOK_URI + '/getbookinfo/:id', API_ADDBOOK_URI.getbookinfo);
    app.get(API_ADDBOOK_URI + "/getbookinfo/:id", function(req, res) {
        var goodReadsRequest = GOODREADS_BOOKS_URL + 
            "?key=" + GOODREADS_KEY + 
            "&id=" + req.params.id;

        request(goodReadsRequest, 
                function (error, response, body) {
                    //console.log('body:', body); 
                    parseString(body, function (err, result) {
                        //console.log(JSON.stringify(result.GoodreadsResponse.search));
                        res.json(result.GoodreadsResponse);
                    });
                });
    });

    app.get("/api/users/books/:userid", function(req,res){
        // var books = database.Books;
        var Users = database.Users;
        var Book_Users = database.Book_Users;
        var Books = database.Books;
        
        var userid = req.session.userProfile.user[0].userid;   
        
        console.log("getting books from userid," + userid);

        Users
            .findAll({
                where: {userid: userid}
                ,limit: 1
                ,include: [{
                    model: Book_Users
                    // ,limit: 20
                    ,include: [Books]
                }]
            })
            .then(function(result){
                res
                    .status(200)
                    .json(result);
                    //console.log(result);
            })
            .catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    });  // Getting books added by User:userid

    app.get(API_ADDBOOK_URI + "/getPublisherName/:pubid", function(req,res){
        var Publishers = database.Publishers;
        var publisherName = "";
            Publishers
                .findAll({

                })
                .then(function(result){
                    publisherName = result[0].publishername;
                    res
                        .status(200)
                        .send(publisherName);
                })
                .catch(function(err){
                    res
                        .status(500)
                        .json(err);
                })
    });  // Get Publisher Name by pubid

    app.get(API_ADDBOOK_URI + "/isBookAdded/:isbn10", function(req, res){
        var isbn10 = req.params.isbn10;
        var Users = database.Users;
        var Book_Users = database.Book_Users;
        var Books = database.Books;
        var userid = req.session.userProfile.user[0].userid;   
        var bookAdded = 0;

        var SQL_QUERY = 
            "select COUNT(`isbn10`) AS `added` from books " +
            "inner join book_users on books.bookid = book_users.bookid " + 
            "inner join users on book_users.userid = users.userid " +
            "where books.isbn10 = ? and users.userid = ?;"

        //console.log("@@@@isBookAdded@@@@ isbn10>>>",isbn10);
        database.conn.query(SQL_QUERY,
            { replacements: [ isbn10 , userid ], type: Sequelize.QueryTypes.SELECT}
        ).then(Books => {
            // console.log(Books[0].added);
            bookAdded = Books[0];
            // console.log(bookAdded);
            res.json(bookAdded);
        })
    });  // Check is the Book with the isbn10 has been added by user, return 1/0

    app.post(API_ADDBOOK_URI + "/addBook", function(req, res){
        var book = req.body.book;
        var Books = database.Books;
        var Book_Users = database.Book_Users;
        var userid = req.session.userProfile.user[0].userid;  
        var bookid = 0;
        // console.log("in addBook app.post");
        // console.log(book);
        
        // console.log("goodreadsid: " ,book.id[0]);
        // console.log("isbn10: ",book.isbn[0]);
        // console.log("isbn13: ",book.isbn13[0]);
        // console.log("title: ", book.title[0]);
        // console.log("author: ",book.authors[0].author[0].name[0]);
        // console.log("format: ",book.format[0]);
        // console.log("imgurl: ", book.image_url[0]);
        // console.log("synopsis: ",book.description[0]);

        database.conn.transaction(function(t){
            return Books.findOrCreate({
                    where: {
                        goodreadsid: book.id[0],
                        isbn10: book.isbn[0],
                        isbn13: book.isbn13[0],
                        title: book.title[0],
                        author: book.authors[0].author[0].name[0],
                        format: book.format[0],
                        imgurl: book.image_url[0],
                        synopsis: book.description[0]
                        //Pubid: 2
                        },
                    transaction: t
                })
                .then(function(book){
                    // console.log("inner result " + JSON.stringify(book));
                    // console.log("##############");
                    // console.log(book[0].bookid);
                    bookid = book[0].bookid;
                    return Book_Users.create({
                        bookid: bookid,
                        userid: userid,
                        status: 'read'
                    }, {transaction: t});
                })
        })
        .then(function(book){
            res
            .status(200)
            .json(book);
        })
        .catch(function(err){
            console.log("Add book error >>> ", err);
            res
            .status(501)
            .json(err);
        });
    }); // addBooks 
        
    // route middleware to make sure a user is logged in
    function isLoggedIn(req, res, next) {
            // if user is authenticated in the session, carry on 
            console.log("isLoggedIn");
            console.log("req.isAuthenticated >>",req.isAuthenticated());
            if (req.isAuthenticated()){
                //console.log("req.isAuthenticated >>",req.isAuthenticated());
                return next();
            }
            // if they aren't redirect them to the home page
            return res.redirect(SIGNIN_PAGE);
    }

    app.use(function(req, res, next) {
        console.log("I am the last app.use ");
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};



    /*  /api/users/updateprofile --> Update Profile  */
    // app.post(API_USERS_URI + "/updateprofile", function(req, res) {
    //     var user = database.Users;

    //     var userid = req.body.user.userid;
        
    //     var password = req.body.user.password;
    //     var hashpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        
    //     var email = req.body.user.email;
    //     var firstname = req.body.user.firstname;
    //     var lastname = req.body.user.lastname;
        
    //     var gender = req.body.user.gender;
    //     var dob = req.body.user.dob;
    //     var country = req.body.user.country;
    //     var profilephoto = req.body.user.profilephoto;
    //     // console.log("req.body.user>>>>>>");
    //     // console.log(req.body.user);
    //     // database.Users
    //     user
    //         .find({where: { userid: userid}})
    //         .then(function(rUser){
    //             console.log(">>>>>rUser >>>>");
    //             console.log(rUser);
    //             rUser.updateAttributes({
    //                 email: email,
    //                 firstname: firstname,
    //                 lastname: lastname,
    //                 password: hashpassword,
    //                 gender: gender,
    //                 dob: dob,
    //                 country: country,
    //                 profilephoto: profilephoto
    //             })
    //             .then(function(results) {
    //                 //console.log(">>>>/api/users/updateprofile >>>>>");
    //                 //console.log(results);
    //                 res
    //                     .status(200)
    //                     .json(rUser);
    //             })
    //             .catch(function(err) {
    //                 res
    //                     .status(501)
    //                     .json(err) 
    //             });
    //         })
    //         .catch(function(err){
    //             res
    //             .status(501)
    //             .json(err) 
    //         })
            
    // }); //Update Profile