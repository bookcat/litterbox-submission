var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('book_users', {
        bookid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
        },
        userid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
        },    
        status: {
            type:Sequelize.ENUM('read','reading','unread'),
            allowNull: false
        }
    });
};