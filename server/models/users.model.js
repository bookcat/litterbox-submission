var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('users', {
        userid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type:Sequelize.STRING,
            allowNull: false
        },
        firstname: {
            type:Sequelize.STRING,
            allowNull: false
        },
        lastname: {
            type:Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: true
        },
        gender: {
            type:Sequelize.ENUM('m', 'f'),
            defaultValue:'f',
            allowNull: false
        },
        dob: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        country: {
            type: Sequelize.STRING,
            allowNull: false
        },
        profilephoto: {
            type: 'BLOB',
            allowNull: true
        },
        authprovider: {
            type: Sequelize.STRING,
            allowNull: true
        }
        // ,
        // roleid: {
        //     type : Sequelize.INTEGER(2),
        //     allowNull: false
        // }
        // ,
        // createdat: {
        //     type: Sequelize.DATE,
        //     allowNull: false
        // },
        // updatedat: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // },
        // deletedat: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // }
    });
};