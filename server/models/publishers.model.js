var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('publishers', {
        pubid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        publishername: {
            type:Sequelize.STRING,
            allowNull: false
        }
    });
};