var Sequelize = require('sequelize');
var config = require("./config");

//console.log(config.mysql);
var db = new Sequelize(config.mysql, {
    pool: {
        max: 4,
        min: 1,
        idle: 10000,
        dialect:'mysql',
        acquire: 10000
    }
});

var Users = require("./models/users.model.js")(db);
var Roles = require("./models/roles.model.js")(db);
var Books = require("./models/books.model.js")(db);
var Publishers = require("./models/publishers.model.js")(db);

var Book_Users = require("./models/book_user.model.js")(db);

//var AuthProvider = require("./models/authentication.provider.model.js")(db);

// BEGIN: MYSQL RELATIONS

Users.belongsTo(Roles, {as: '_'});
Books.belongsTo(Publishers, {as: '_'});
//Publishers.hasMany(Books, {foreignKey: 'pubid'});
// Users.belongsToMany(Books, { through: Book_User, foreignKey: 'userid' });
// Books.belongsToMany(Users, { through: Book_User, foreignKey: 'bookid' });

Users.hasMany(Book_Users, {foreignKey: 'userid'})
//DeptEmp.hasOne(Department, {foreignKey: 'dept_no'});
Book_Users.belongsTo(Books, {foreignKey: 'bookid'});

// END: MYSQL RELATIONS

db
.query('SET FOREIGN_KEY_CHECKS = 0', {raw: true})
     .then(function (results) {
        db.sync({force: config.seed})
        .then(function () {
            console.log("Database in Sync Now");
            require("./seed")();
        });
    });

module.exports = {
    conn: db,
    Users: Users,
    Roles: Roles,
    Books: Books,
    Publishers: Publishers,
    Book_Users: Book_Users
};

