(function() {
    angular
        .module("BookCatApp")
        // .run(function($rootScope){
        //     $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
        //         console.log(userDetails);
        //     })
        // })
        .controller("LoginCtrl", ["$http", "$state", "AuthService", LoginCtrl]);

        function LoginCtrl($http, $state, AuthService) {
            var vm = this;

            var init = function(){
                vm.message ="";
                $http.get("/getErrorMessage", {
                }).then(function(result){
                    console.log(result.data);
                    vm.message = result.data;
                    //vm.message="Welcome back! We've found your email in our database. Please login above or reset password.";
                    $state.$current
                }).catch(function(err){
                    console.log("err >>>",err);
                })
            };
            init();
            vm.login = function() {
            
                AuthService.login(vm.user)
                    .then(function(result) {
                        
                        if (result.status == 200) {
                            console.log("login successful>>>>>,", result);
                            var loginEmail = result.config.data.email;
                            console.log("Welcome, ", loginEmail);
                            $location.path('/');
                            //window.location="/app/protected/home.html";
                            // $state.go('userhome', { email: loginEmail });
                        };
                        // if(AuthService.isUserLoggedIn()){
                        //     $state.go("home");
                        // }else{
                        //     $state.go("form");//$state.go("login");
                        // };
                    }).catch(function(err) {
                        console.log("err returned from authService.login");
                        console.log(err);
                        if (err.status == 401) {
                            vm.message = err.data.err;//"Invalid email or password."                   
                        } else if (err.status == 500) {
                            vm.message = "Could not log in user at this time."                                           
                        } else {
                            // it is some other server-returned error
                            vm.message = "Unexpected server error"                                           
                            console.log("Error: ", err);
                        }
                        console.log("message >>>", vm.message);
                        $state.go('form');
                    });
            }
        }
}) ();