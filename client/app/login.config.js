(function() {
    angular
        .module("BookCatApp")
        .config(bookCatLoginConfig);
    
    bookCatLoginConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    console.log("in loginrouter")
    

    function bookCatLoginConfig($stateProvider, $urlRouterProvider) {        

        $urlRouterProvider.otherwise("/");
        // $urlRouterProvider.when("/mybooks", "/mybooks/catalogue");
        
        $stateProvider
            .state("home", {
                url:"/",
                views:{
                    "searchbook":{ templateUrl: "searchbook.html" },
                    "books":{ templateUrl: "catalogue.html", 
                        controller: "CatalogCtrl",
                        views: {  "bookdetail": { templateUrl: "bookdetail.html" } }
                    },
                    "recommendations":{ templateUrl: "recommendations.html" },
                    // "bookdetail": { templateUrl: "bookdetail.html" }
                }
            })
            .state("account", {
                url:"/account",
                templateUrl:"account.html"
            })
            .state("account.profile", {
                url:"/profile",
                templateUrl:"profile.html"
            })
            .state("account.password", {
                url:"/password",
                templateUrl:"password.html"
            })
            .state("mybooks", {
                url:"/mybooks",
                templateUrl:"mybooks.html"
            })
            .state("mybooks.catalogue", {
                url:"/mybooks/catalogue",
                views:{
                    "catalogue": {templateUrl:"catalogue.html"},
                    "lists": {templateUrl: "lists.html"},
                    "bookdetail": { templateUrl: "bookdetail.html" }                    
                }
            })
            .state("catalogue", {
                url:"/catalogue",
                views:{
                    "bookdetail": { templateUrl: "bookdetail.html" }                                        
                }
            })
        }
}) ();