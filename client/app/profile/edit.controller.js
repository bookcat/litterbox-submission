"use strict";

(function() {
    angular
        .module("BookCatApp")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter","$stateParams", "UserSvc"];
    

    function EditCtrl($filter, $stateParams, UserSvc) {
        var vm = this;

        vm.emp_no = "";
        vm.result = {};

        vm.initDetails = initDetails;
        vm.updateFirstName = updateFirstName;
        vm.updateLastName = updateLastName;
        vm.updateFirstName = updateFirstName;
        vm.toggleEditor = toggleEditor;

        function initDetails() {
            vm.result.email = "";
            vm.result.firstname = "";
            vm.result.lastname = "";
            vm.result.gender = "";
            vm.result.dob = "";
            vm.result.country = "";
            vm.showDetails = false;
            vm.isEditorOn = false;

        };

        function updateFirstName() {
            UserSvc
                .updateFirstName ( vm.result.firstname, vm.result.lastname)
                .then(function (result) {
                    console.log("-- show.controller.js > updateFirstName() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > updateFirstName() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

    }
}) ();