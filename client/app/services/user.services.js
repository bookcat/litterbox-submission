(function() {
    angular
        .module("BookCatApp")
        .service("UserSvc", ["$http", "$q", UserSvc]);

    function UserSvc($http, $q) {
        var service = this;

        service.registerUser = registerUser;

        function registerUser(user) {
            console.log(">>>>in registerUser>>");
            var defer = $q.defer();
            
            $http.post("/api/users/register", {user: user},{
            }).then(function(result){
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
            // return $http({
            //     method: 'POST',
            //     url: '/api/users/register',
            //     data: { user: user }
            // });
        };

        service.checkEmail = checkEmail;
        ///api/users/checkemail/:email
        function checkEmail(email) {
            var defer = $q.defer();
            console.log(">>>>in checkEmail>>");
            $http.get("/api/users/checkemail/" + email, {})
            .then(function(result){
                defer.resolve(result);
            }).catch(function(err){
                defer.reject(err);
            });
            return defer.promise;
        };

        service.getUserProfile = getUserProfile;
        
                function getUserProfile(userid) {
                    var defer = $q.defer();
                    $http.get("/api/users/getuserprofile/" + userid, {
                    }).then(function(result){
                        console.log(result.data[0]);
                        defer.resolve(result);
                    }).catch(function(err){
                        defer.resolve(err);
                    })
                    return defer.promise;
                };
        
                service.saveUserPost = saveUserPost;
                
                        function saveUserPost(userProfile) {
                            console.log("I am SaveUser service>>>, userProfile", userProfile);
                            var defer = $q.defer();
                            $http.post("/api/users/updateprofile", { userProfile },{
                            }).then(function(result){
                                console.log(result.data[0]);
                                defer.resolve(result);
                            }).catch(function(err){
                                defer.resolve(err);
                            })
                            return defer.promise;
                        };


    }


})();