(function(){
    angular
        .module("BookCatApp")
        .service("CatalogSvc", ["$http", "$q", CatalogSvc]);

        function CatalogSvc($http, $q){
            var catalogSvc = this;
            
            // var userid = req.session.userProfile.userid;
            var userid = 2;

            catalogSvc.getUserBooks = function(userid){
                var defer = $q.defer();
                $http.get("/api/users/books/" + userid, {
                }).then(function(result){
                    // console.log("==============================");
                    //     console.log("in Catalog.service");
                    //     console.log(result);
                        defer.resolve(result);
                    }).catch(function(err){
                        defer.reject(err);
                    })
                return defer.promise;
            };

            catalogSvc.getPublisherName = function(pubid){
                var defer = $q.defer();
                $http.get("/api/getPublisherName/" + pubid, {
                }).then(function(result){
                    // console.log("==============================");
                    //     console.log("in Catalog.service");
                    //     console.log(result);
                        defer.resolve(result);
                    }).catch(function(err){
                        defer.reject(err);
                    })
                return defer.promise;
            };
        }
}) ();