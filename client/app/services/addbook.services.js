(function(){
    angular
    .module("BookCatApp")
    .service("BookApiSvc", ["$http","$q",BookApiSvc]);

    function BookApiSvc($http, $q){
        var bookApiSvc = this;

        // searchBook function
        bookApiSvc.searchBook = function(searchStr) {
            var defer = $q.defer();
            console.log (">>>> in Services, %s", searchStr)
            $http.get("/api/searchbook/" + searchStr, {
            }).then(function(result){
                    //console.log(result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
            
        }; // searchBook function

        // getBookInfo function
        bookApiSvc.getBookInfo = function (id){
            var defer = $q.defer();
            $http.get("/api/getbookinfo/" + id, {
            }).then(function(result){
                    //console.log(result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
        }; // getBookInfo function


        bookApiSvc.isBookAdded = function(isbn10){
            var defer = $q.defer();
            $http.get("/api/isBookAdded/" + isbn10, {
            }).then(function(result){
                    // console.log("IsBookAdded >> ",result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
        }  // Return BookAdded == TRUE / FALSE

        bookApiSvc.addBook = function(book){
            var defer = $q.defer();
            $http.post("/api/addBook/", 
            { book: book },{
            }).then(function(result){
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
        }
    } //BookApiSvc
}) ();

