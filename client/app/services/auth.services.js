(function () {
    angular.module("BookCatApp")
    .service("AuthService", [
        "$http",
        "$q",
        AuthService
    ]);

    function AuthService($http, $q) {
        var authService = this;
        var user = null;
        authService.register = function(){

        }

        authService.login = function(userProfile){
            console.log(">>>> In AuthService.login >>>>");
            console.log("#####userProfile#####");
            console.log(userProfile);
            // return $http({
            //     method: 'POST',
            //     url: '/api/users/auth',
            //     // passport is expecting 2 values: username (email) and password
            //     data: {
            //         userProfile
            //     }
            // });
            var deferred = $q.defer();
            $http.post("/security/login", userProfile)
                .then(function(data){
                    console.log("data returned from /security/login");
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve(data);
                    }
                }).catch(function (error){
                    // console.log("err returned from /security/login")
                    // console.log(error.status);
                    user = false;
                    deferred.reject(error);
                })
            return deferred.promise;
        }

        authService.getUserStatus = function (callback){ 
            $http.get('/status/user')
            // handle success
                .then(function (data) {
                    var authResult = JSON.stringify(data);
                    if(data["data"] != ''){
                        user = true;
                        callback(user);
                    } else {
                        user = false;
                        callback(user);
                    }
                });
        }
        authService.isUserLoggedIn = function(){
            var deferred = $q.defer();
            $http.get("/status/user").then(function(data){
                // console.log("###$$$%%%%% in auth.services.js, authService.isUserLoggedIn ###$$$%%%%% ");
                // console.log(">>>>>start printing data in authService.isUserLoggedIn");
                // console.log(data);
                // console.log(">>>>>end of printing data in authService.isUserLoggedIn");
                user = true;
                //return(user);
                deferred.resolve(data);
            }).catch(function(error){
                console.log(error);
                user= false;
                deferred.reject(error);
                //cb(user);
            })
            return deferred.promise;
        }

        authService.logout = function(){
            var deferred = $q.defer();
            $http.get("/logout")
                .then(function(data){
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve();
                    }
                }).catch(function (error){
                    console.log(error);
                    user = false;
                    deferred.reject(error);
                })
            return deferred.promise;
        }
    }
})();   
