(function() {
    angular
        .module("BookCatApp")
        .config(bookCatRouteConfig);
    
    bookCatRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];


    function bookCatRouteConfig($stateProvider, $urlRouterProvider) {        

        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state("landingpage", {
                url:"/",
                templateUrl: "landingpage.html"
            })
            .state("registration", {
                url:"/registration",
                templateUrl: "form-details.html"
            })
            .state("userhome",{
                url:"/home/:email",
                templateUrl: "/app/protected/home.html"
            })


        }
}) ();