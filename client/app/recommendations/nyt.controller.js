(function(){

    angular
    .module("BookCatApp")
    .service("NytSvc", ["$http", "$q", NytSvc])
    .controller("NytCtrl", ["$state", "NytSvc", NytCtrl]);
    
        var NytSvc = function($http, $q){
            var nytSvc = this;
    
            nytSvc.getNytList = function(url,key) {
                var defer = $q.defer();
                $http.get(url, {
                    params: { "api-key": key }
                }).then(function(result){
                    console.log(result);
                    var overview = result.data.results.lists;
                    defer.resolve(overview);
                }).catch(function(err){
                    defer.reject(err);
                })
                return (defer.promise);
            }
        }
        NytSvc.$inject = ["$http", "$q"];
        
        var NytCtrl = function($state, NytSvc){
            var nytCtrl = this;
            var urlOverview = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
            var key = "ab31a1ff89a34249b0567ad01ec17db6"; 
            nytCtrl.list = "";
    
            NytSvc.getNytList(urlOverview,key)
                .then(function(result){
                    nytCtrl.list = result;
                    console.log(">>> Book List >>> ", nytCtrl.list);
                }).catch(function(err){
                    console.error(" >>>> getBookList: ", err);
                })
        }
        NytCtrl.$inject = [ "$state", "NytSvc" ];
        
}) ();