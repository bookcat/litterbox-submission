(function(){
    
        angular
            .module("BookCatApp")
            .controller("AddBookCtrl", ["$scope", "$state", "BookApiSvc", AddBookCtrl ]);
    
        function AddBookCtrl($scope, $state, BookApiSvc){
            var addBookCtrl = this;
            
            var checkNoPhoto = "/nophoto/";
            addBookCtrl.queryStr = "";
            addBookCtrl.defaultBookCoverPath = "../../assets/images/defaultbookcover.png";

            $("#searchModal").on("hidden.bs.modal", function () {
                $state.transitionTo($state.current, {seqId: ''}, { reload: true});
            });

            var initValues = function (){
                var lenOfResult = 0;
                var bookId = 0;
                var searchResult = [];
                var image_url = "";
                var isbn10 = "";
                addBookCtrl.searchResultCount = 0;
                addBookCtrl.selectedAll = false;
                addBookCtrl.booksGoodReads = [];
                addBookCtrl.booksSelect = [];
                addBookCtrl.addFlag = [];
            }
            
            initValues();

            addBookCtrl.search = function(){

                $scope.isLoading = true;

                initValues();
                BookApiSvc.searchBook(addBookCtrl.queryStr)
                .then(function(result){
                    //console.log("Results End >>> " + result.data[0]["total-results"]);
                    addBookCtrl.searchResultCount = result.data[0]["total-results"];
                    if (addBookCtrl.searchResultCount > 0) {
                        searchResult = result.data[0].results[0].work;
                        

                        lenOfResult = searchResult.length;
                        for(var i = 0; i < lenOfResult; i++){
                            //console.log("i = %d --> Rating: %s", i , addBookCtrl.booksGoodReads[i].average_rating[0]);
                            bookId = searchResult[i].best_book[0].id[0]._;
                            //console.log("bookId >>>" +  bookId);
    
                            //GET https://www.goodreads.com/book/show.xml?key={developer_key}&id={book_id}
                            BookApiSvc.getBookInfo(bookId)
                                .then(function(resultBookInfo){
                                    $scope.isLoading = false;
                                    
                                    /*
                                        call a function to check is the resultBookInfo exist in the 
                                        database, table: books, books_user using isbn as reference
                                        if yes, addBookCtrl.addFlag.push = true, 
                                        else addBookCtrl.addFlag.push = false
                                    */
                                    isbn10 = resultBookInfo.data.book[0].isbn;
                                    //console.log("isbn10 >>",isbn10);
                                    if (isbn10 != ''){
                                    BookApiSvc.isBookAdded(isbn10)
                                        .then(function(result){
                                            //console.log("result.added >>", result.data.added);
                                                addBookCtrl.addFlag.push(result.data.added);
                                        }).catch(function(err){
                                            console.log(err);
                                        })
                                    }
                                    // Check resultBookInfo.data.book[0].image_url[0]
                                    image_url = resultBookInfo.data.book[0].image_url[0];
                                    if (image_url.indexOf(checkNoPhoto) >= 0 ){
                                        resultBookInfo.data.book[0].image_url[0] = addBookCtrl.defaultBookCoverPath;
                                    }

                                    if( resultBookInfo.data.book[0].isbn[0] != "") {
                                        addBookCtrl.booksGoodReads.push(resultBookInfo.data.book[0]) ;
                                    }
                                    // console.log("addBookCtrl.booksGoodReads >>>> " , addBookCtrl.booksGoodReads);
                                    // console.log("addBookCtrl.addFlag>>>",addBookCtrl.addFlag);

                                }).catch(function(err){
                                    console.log(err);
                                })
                            }
                    } else { // lenOfResult <0
                        console.log("No Result");
                        initValues();
                        //console.log(result);
                    }
                    
                }).catch(function (err){
                    console.error(err);
                })

                
            }; //addBookCtrl.search
            
            addBookCtrl.selectAll = function(){
                var i = 0;
                console.log("Select all checked/unchecked");
                if (addBookCtrl.selectedAll){
                    addBookCtrl.selectedAll = true;
                } else {
                    addBookCtrl.selectedAll = false;
                }
                angular.forEach(addBookCtrl.booksGoodReads, function (item) {
                    if (addBookCtrl.addFlag[i] == 0){
                        item.Selected = addBookCtrl.selectedAll;
                    }
                    i=i+1;
                });
            } // addBookCtrl.selectAll
            
            addBookCtrl.addBooks = function(){
                var i = 0;
                console.log("addBooks button clicked");
                angular.forEach(addBookCtrl.booksGoodReads, function (item) {
                    
                    if(item.Selected){
                        console.log("i >> %d", i, " item >> ", item);
                        BookApiSvc.addBook(addBookCtrl.booksGoodReads[i])
                        .then(function(result){
    
                            console.log("Add book complete!");
                            addBookCtrl.search();
                        }).catch(function(err){
                            console.log("addBookCtrl error >>> ", err);
                        })
                    }   
                    i=i+1;
                });
            } // addBookCtrl.addBooks

            addBookCtrl.addBook = function(index){
                console.log("addBook button clicked");
                console.log("Clicked %d", index, ", %s ", addBookCtrl.booksGoodReads[index].title[0]);
                BookApiSvc.addBook(addBookCtrl.booksGoodReads[index])
                    .then(function(result){

                        console.log("Add book complete!");
                        addBookCtrl.search();
                    }).catch(function(err){
                        console.log("addBookCtrl error >>> ", err);
                    })

            } // addBookCtrl.addBook

        };  //AddBookCtrl
    
})();