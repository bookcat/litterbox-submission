(function(){
    angular
        .module("BookCatApp")
        .controller("CatalogCtrl", CatalogCtrl)
        .controller("EditCatalogCtrl", EditCatalogCtrl);

    CatalogCtrl.$inject = ['$state','CatalogSvc', 'AuthService', '$document','$uibModal'];
    EditCatalogCtrl.$inject = ['$state','CatalogSvc', '$uibModalInstance', 'item'];
    
    
    function EditCatalogCtrl ($state, CatalogSvc, $uibModalInstance, item){
        var EditCatalogCtrl = this;
        console.log("item >>>",item);
        EditCatalogCtrl.title = item.title;
        EditCatalogCtrl.author = item.author;
        EditCatalogCtrl.isbn10 = item.isbn10;
        EditCatalogCtrl.imgurl = item.imgurl;
        EditCatalogCtrl.synopsis = item.synopsis;
        EditCatalogCtrl.status = item.status;
        //===========================================
        EditCatalogCtrl.pubid = item.pubid;
        
        EditCatalogCtrl.publishername = item.publishername; 
        // console.log("EditCatalogCtrl.publishername >> ",EditCatalogCtrl.publishername );
       //============================================
    }
    
    function CatalogCtrl($state, CatalogSvc, AuthService, $document, $uibModal){
        var catalogCtrl = this;
        catalogCtrl.userid = 0;

        //=====================================================================//
        // Get Logged In UserId
        //=====================================================================//
        var getUserId = function(){
            AuthService.isUserLoggedIn()
            .then(function(result){
                if(result != ""){
                    // console.log("CatalogCtrl.getUserId >>>");
                    // console.log(result.data.userid);
                    catalogCtrl.userid = result.data.userid;
                    console.log("UserId: %d is logged in.", catalogCtrl.userid);
                } else {
                    window.location = "/";
                }
            }).catch(function(err){
                console.log("Error in CatalogCtrl getUserId >>> ", err);
            })
        }
        getUserId();
        //=====================================================================//

        getBooks();

        function getBooks(){
            CatalogSvc.getUserBooks(catalogCtrl.userid)
            .then(function(results){
                catalogCtrl.books = results.data[0].book_users;
                console.log(catalogCtrl.books);
            })
            .catch(function(err){
                console.log("error in getBooks >>> " + err);
            })
        };

        catalogCtrl.clear = function(){
            console.log("clear function")
        }


        // function viewBookDetails(isbn) {
        //     catalogCtrl.books.books.title="";
        //     catalogCtrl.books.book.author="some author";
        //     catalogCtrl.books.book.synopsis="";
        // };

        // function editBookStatus(){

        // }

        catalogCtrl.viewBookDetails = function(size, idx ,parentSelector){
            console.log("clear function");
            
            var parentElem = parentSelector ? angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: catalogCtrl.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'bookdetail.html',
                controller: 'EditCatalogCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    item: function () {
                        console.log("ddddd " + idx);
                        var selectedbook = catalogCtrl.books[idx];
                        
                        // selectedbook.book.publishername = "";   //========
                        
                        console.log(">>>> " + selectedbook.book.title);
                        //console.log(">>>> pubid " + selectedbook.book.pubid);
                        //==============================================================
                        //Get publisher name
                        // var getPublisherName = function(){
                        //     CatalogSvc.getPublisherName(selectedbook.book.pubid)
                        //         .then(function(result){
                        //             console.log(">>>PublisherName>>>", result.data);
                        //             selectedbook.book.publishername = result.data;
                                    
                        //         }).catch(function(err){
                        //             console.log("Err in getPublisherName >>>",err);
                        //         })
                        // };
                        // getPublisherName();
                        //==============================================================
                        return selectedbook.book;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
            });
        }

    }
    
    
}) ();