(function () {
    angular
        .module("BookCatApp")
        .controller("HomeCtrl", ["$q", "AuthService", "$rootScope", "$state", "$location", "$http", HomeCtrl]);

    function HomeCtrl ($q, AuthService, $rootScope, $state, $location, $http){
        var homeCtrl = this;

        homeCtrl.userid = 0;
        homeCtrl.firstName = "";
        AuthService.getUserStatus(function(result){
            //console.log("AuthService.getUserStatus, result >>> ",result);
            homeCtrl.isUserLogon = result;
        });
        function init(){
            console.log("I am init in home controller");
            $http.get("/getUserSignUpProfile", {
            }).then(function(result){
                console.log(result);
                homeCtrl.firstName = result.data[0].firstname;
            }).catch(function(err){
                console.log("err >>>",err);
            })
        };
        init();

        homeCtrl.isUserLogon = function(){
            AuthService.isUserLoggedIn()
            .then(function(result){
                if(result != ""){
                    // console.log("homeCtrl.isUserLogon >>>> result");
                    // console.log(result.data.userid);
                    homeCtrl.userid = result.data.userid;
                    console.log("I am home controller >> UserId: %d is logged in.", homeCtrl.userid);
                } else {
                    window.location = "/";
                }
            }).catch(function(err){
                console.log("Error in HomeCtrl isUserLogon >>> ", err);
            })
        };
        homeCtrl.isUserLogon();

        homeCtrl.logout = function(){
            AuthService.logout()
                .then(function() {
                    console.log("User Logged Out")
                    window.location = "/";
                }).catch(function(err) {
                    console.error("Error Logging Out >>>>", err);
                })
        }
       
        // homeCtrl.isUserLogon = AuthService.isUserLoggedIn();
        // $rootScope.$watch('user', function() {
        // 	console.log("homeCtrl.isUserLogon>>>",homeCtrl.isUserLogon);
        //     console.log("$state.$current.url>>>",$state.$current.url);
        //     if($state.$current.url != "" && !homeCtrl.isUserLogon){
        //         $state.go('index');
        //     }
    	// });

        // var defer = $q.defer();
        // homeCtrl.err = null;

        // homeCtrl.isActive = function (viewLocation) {
        //     //console.log(viewLocation);
        //     console.log($location.path());
        //     return viewLocation === $location.path();
        // };

    }

})();
